#include <QMap>
#include <GFlowKey>
#include <GSyncPcapFile>
#include <GFlowKey>

void usage() {
	printf("syntax : packet-stat <pcap file>\n");
	printf("sample : packet-stat test.pcap\n");
}

// ----------------------------------------------------------------------------
// Endpoint
// ----------------------------------------------------------------------------
struct Stat {
	int txPkts{0};
	int txBytes{0};
	int rxPkts{0};
	int rxBytes{0};
};

struct MacMap : public QMap<GMac, Stat> {
	void process(GPacket* packet) {
		GMac smac = packet->ethHdr_->smac();
		GMac dmac = packet->ethHdr_->dmac();
		MacMap::iterator it;

		it = find(smac);
		if (it == end()) it = insert(smac, Stat());
		it->txPkts++;
		it->txBytes += packet->buf_.size_;

		it = find(dmac);
		if (it == end()) it = insert(dmac, Stat());
		it->rxPkts++;
		it->rxBytes += packet->buf_.size_;
	}

	void print() {
		printf("mac\ttxPkts\ttxBytes\trxPkts\trxBytes\n");
		for (MacMap::iterator it = begin(); it != end(); it++) {
			GMac mac = it.key();
			printf("%s", qPrintable(QString(mac)));
			Stat stat = it.value();
			printf("\t%d\t%d\t%d\t%d\n", stat.txPkts, stat.txBytes, stat.rxPkts, stat.rxBytes);
		}
		printf("\n");
	}
};

struct IpMap : public QMap<GIp, Stat> {
	void process(GPacket* packet) {
		GIp sip = packet->ipHdr_->sip();
		GIp dip = packet->ipHdr_->dip();
		IpMap::iterator it;

		it = find(sip);
		if (it == end()) it = insert(sip, Stat());
		it->txPkts++;
		it->txBytes += packet->buf_.size_;

		it = find(dip);
		if (it == end()) it = insert(dip, Stat());
		it->rxPkts++;
		it->rxBytes += packet->buf_.size_;
	}

	void print() {
		printf("ip\ttxPkts\ttxBytes\trxPkts\trxBytes\n");
		for (IpMap::iterator it = begin(); it != end(); it++) {
			GIp ip = it.key();
			printf("%s", qPrintable(QString(ip)));
			Stat stat = it.value();
			printf("\t%d\t%d\t%d\t%d\n", stat.txPkts, stat.txBytes, stat.rxPkts, stat.rxBytes);
		}
		printf("\n");
	}
};

struct TcpMap : public QMap<GFlow::TcpKey, Stat> {
	void process(GPacket* packet) {
		GFlow::TcpKey sTcpKey(packet->ipHdr_->sip(), packet->tcpHdr_->sport());
		GFlow::TcpKey dTcpKey(packet->ipHdr_->dip(), packet->tcpHdr_->dport());
		TcpMap::iterator it;

		it = find(sTcpKey);
		if (it == end()) it = insert(sTcpKey, Stat());
		it->txPkts++;
		it->txBytes += packet->buf_.size_;

		it = find(dTcpKey);
		if (it == end()) it = insert(dTcpKey, Stat());
		it->rxPkts++;
		it->rxBytes += packet->buf_.size_;
	}

	void print() {
		printf("tcp ip:port\ttxPkts\ttxBytes\trxPkts\trxBytes\n");
		for (TcpMap::iterator it = begin(); it != end(); it++) {
			GFlow::TcpKey tcpKey = it.key();
			printf("%s:%d", qPrintable(QString(tcpKey.ip_)), tcpKey.port_);
			Stat stat = it.value();
			printf("\t%d\t%d\t%d\t%d\n", stat.txPkts, stat.txBytes, stat.rxPkts, stat.rxBytes);
		}
		printf("\n");
	}
};

struct UdpMap : public QMap<GFlow::UdpKey, Stat> {
	void process(GPacket* packet) {
		GFlow::UdpKey sUdpKey(packet->ipHdr_->sip(), packet->udpHdr_->sport());
		GFlow::UdpKey dUdpKey(packet->ipHdr_->dip(), packet->udpHdr_->dport());
		TcpMap::iterator it;

		it = find(sUdpKey);
		if (it == end()) it = insert(sUdpKey, Stat());
		it->txPkts++;
		it->txBytes += packet->buf_.size_;

		it = find(dUdpKey);
		if (it == end()) it = insert(dUdpKey, Stat());
		it->rxPkts++;
		it->rxBytes += packet->buf_.size_;
	}

	void print() {
		printf("udp ip:port\ttxPkts\ttxBytes\trxPkts\trxBytes\n");
		for (TcpMap::iterator it = begin(); it != end(); it++) {
			GFlow::TcpKey tcpKey = it.key();
			printf("%s:%d", qPrintable(QString(tcpKey.ip_)), tcpKey.port_);
			Stat stat = it.value();
			printf("\t%d\t%d\t%d\t%d\n", stat.txPkts, stat.txBytes, stat.rxPkts, stat.rxBytes);
		}
		printf("\n");
	}
};

// ----------------------------------------------------------------------------
// Flow
// ----------------------------------------------------------------------------
struct FlowStat {
	int txPkts{0};
	int txBytes{0};
};

struct EthFlowMap : public QMap<GFlow::MacFlowKey, FlowStat> {
	void process(GPacket* packet) {
		GFlow::MacFlowKey macFlowKey(packet->ethHdr_->smac(), packet->ethHdr_->dmac());
		EthFlowMap::iterator it = find(macFlowKey);
		if (it == end()) it = insert(macFlowKey, FlowStat());
		it->txPkts++;
		it->txBytes += packet->buf_.size_;
	}

	void print() {
		printf("smac dmac\ttxPkts\ttxBytes\trxPkts\trxBytes\n");
		for (EthFlowMap::iterator it = begin(); it != end(); it++) {
			GFlow::MacFlowKey macFlowKey = it.key();
			printf("%s %s", qPrintable(QString(macFlowKey.smac_)), qPrintable(QString(macFlowKey.dmac_)));
			FlowStat flowStat = it.value();
			FlowStat revFlowStat;
			EthFlowMap::iterator revIt = find(macFlowKey.reverse());
			if (revIt != end()) {
				revFlowStat = revIt.value();
				erase(revIt);
			}
			printf("\t%d\t%d\t%d\t%d\n", flowStat.txPkts, flowStat.txBytes, revFlowStat.txPkts, revFlowStat.txBytes);
		}
		printf("\n");
	}
};

struct IpFlowMap : public QMap<GFlow::IpFlowKey, FlowStat> {
	void process(GPacket* packet) {
		GFlow::IpFlowKey ipFlowKey(packet->ipHdr_->sip(), packet->ipHdr_->dip());
		IpFlowMap::iterator it = find(ipFlowKey);
		if (it == end()) it = insert(ipFlowKey, FlowStat());
		it->txPkts++;
		it->txBytes += packet->buf_.size_;
	}

	void print() {
		printf("sip dip\ttxPkts\ttxBytes\trxPkts\trxBytes\n");
		for (IpFlowMap::iterator it = begin(); it != end(); it++) {
			GFlow::IpFlowKey ipFlowKey = it.key();
			printf("%s %s", qPrintable(QString(ipFlowKey.sip_)), qPrintable(QString(ipFlowKey.dip_)));
			FlowStat flowStat = it.value();
			FlowStat revFlowStat;
			IpFlowMap::iterator revIt = find(ipFlowKey.reverse());
			if (revIt != end()) {
				revFlowStat = revIt.value();
				erase(revIt);
			}
			printf("\t%d\t%d\t%d\t%d\n", flowStat.txPkts, flowStat.txBytes, revFlowStat.txPkts, revFlowStat.txBytes);
		}
		printf("\n");
	}
};

struct TcpFlowMap : public QMap<GFlow::TcpFlowKey, FlowStat> {
	void process(GPacket* packet) {
		GFlow::TcpFlowKey tcpFlowKey(packet->ipHdr_->sip(), packet->tcpHdr_->sport(), packet->ipHdr_->dip(), packet->tcpHdr_->dport());
		TcpFlowMap::iterator it = find(tcpFlowKey);
		if (it == end()) it = insert(tcpFlowKey, FlowStat());
		it->txPkts++;
		it->txBytes += packet->buf_.size_;
	}

	void print() {
		printf("tcp sip:sport dip:dport\ttxPkts\ttxBytes\trxPkts\trxBytes\n");
		for (TcpFlowMap::iterator it = begin(); it != end(); it++) {
			GFlow::TcpFlowKey tcpFlowKey = it.key();
			printf("%s:%d %s:%d", qPrintable(QString(tcpFlowKey.sip_)), tcpFlowKey.sport_, qPrintable(QString(tcpFlowKey.dip_)), tcpFlowKey.dport_);
			FlowStat flowStat = it.value();
			FlowStat revFlowStat;
			TcpFlowMap::iterator revIt = find(tcpFlowKey.reverse());
			if (revIt != end()) {
				revFlowStat = revIt.value();
				erase(revIt);
			}
			printf("\t%d\t%d\t%d\t%d\n", flowStat.txPkts, flowStat.txBytes, revFlowStat.txPkts, revFlowStat.txBytes);
		}
		printf("\n");
	}
};

struct UdpFlowMap : public QMap<GFlow::TcpFlowKey, FlowStat> {
	void process(GPacket* packet) {
		GFlow::UdpFlowKey udpFlowKey(packet->ipHdr_->sip(), packet->udpHdr_->sport(), packet->ipHdr_->dip(), packet->udpHdr_->dport());
		UdpFlowMap::iterator it = find(udpFlowKey);
		if (it == end()) it = insert(udpFlowKey, FlowStat());
		it->txPkts++;
		it->txBytes += packet->buf_.size_;
	}

	void print() {
		printf("udp sip:sport dip:dport\ttxPkts\ttxBytes\trxPkts\trxBytes\n");
		for (UdpFlowMap::iterator it = begin(); it != end(); it++) {
			GFlow::UdpFlowKey udpFlowKey = it.key();
			printf("%s:%d %s:%d", qPrintable(QString(udpFlowKey.sip_)), udpFlowKey.sport_, qPrintable(QString(udpFlowKey.dip_)), udpFlowKey.dport_);
			FlowStat flowStat = it.value();
			FlowStat revFlowStat;
			UdpFlowMap::iterator revIt = find(udpFlowKey.reverse());
			if (revIt != end()) {
				revFlowStat = revIt.value();
				erase(revIt);
			}
			printf("\t%d\t%d\t%d\t%d\n", flowStat.txPkts, flowStat.txBytes, revFlowStat.txPkts, revFlowStat.txBytes);
		}
		printf("\n");
	}
};

int main(int argc, char *argv[]) {
	if (argc != 2) {
		usage();
		return -1;
	}

	GSyncPcapFile file;
	file.fileName_ = argv[1];
	if (!file.open()) {
		qDebug() << file.err->msg();
		return -1;
	}

	MacMap macMap; EthFlowMap ethFlowMap;
	IpMap  ipMap;  IpFlowMap  ipFlowMap;
	TcpMap tcpMap; TcpFlowMap tcpFlowMap;
	UdpMap udpMap; UdpFlowMap udpFlowMap;
	while (true) {
		GEthPacket packet;
		GPacket::Result res = file.read(&packet);
		if (res != GPacket::Ok) break;
		macMap.process(&packet);
		ethFlowMap.process(&packet);
		if (packet.ethHdr_->type() == GEthHdr::Ip4) {
			ipMap.process(&packet);
			ipFlowMap.process(&packet);
			switch (packet.ipHdr_->p()) {
				case GIpHdr::Tcp:
					tcpMap.process(&packet);
					tcpFlowMap.process(&packet);
					break;
				case GIpHdr::Udp:
					udpMap.process(&packet);
					udpFlowMap.process(&packet);
					break;
			}
		}
	}
	macMap.print();
	ipMap.print();
	tcpMap.print();
	udpMap.print();
	ethFlowMap.print();
	ipFlowMap.print();
	tcpFlowMap.print();
	udpFlowMap.print();
}
